import { LitElement, html } from "lit-element";

class VivaSendApi extends LitElement {

    static get properties() {
        return {
            person: {type: Object},
            reward: {type: Object}
        }
    }

    constructor() {
        super();

    }

    render() {
        return html`
        `;
    }

    // Se ejecuta al cambiar el valor de alguna de las propiedades
    updated(changedProperties) {
        console.log("updated viva-send-api");

        if (changedProperties.has("person")) {
            this.postPersonData();
        }
    }

    postPersonData() {
        console.log("postPersonData");
        console.log("Enviando datos de la persona");

        let data = JSON.stringify({ 
            "id": this.person.userid,
            "nombre": this.person.name, 
            "apellidos": this.person.surname,
            "edad": this.person.age,
            "fechaAlta": "2011-01-01"
         });
        console.log("Data: ", data);

        let url = "http://localhost:8081/viveawards/users/";

        let xhr = new XMLHttpRequest();

        // Al ser una request asincrona este onload se ejecutará cuando el send (que está más
        // abajo) se complete (responda el API)
        xhr.onload = () => {
            console.log("Status: ", xhr.status)
            if (xhr.status === 201) {
                console.log("Petición completada correctamente");

                //console.log("xhr: ", xhr);
                let APIResponse = JSON.parse(xhr.responseText);

                console.log("APIResponse: ", APIResponse);
                this.reward = APIResponse.results;

                this.dispatchEvent(new CustomEvent("viva-send-api-ok", {}));
            }
        };
        xhr.open("POST",url);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(data);

        console.log("Fin de postPersonData");
    }
}

customElements.define("viva-send-api", VivaSendApi);
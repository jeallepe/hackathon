import { LitElement, html } from "lit-element";

class VivaApiGetUser extends LitElement {

    static get properties() {
        return {
            userid: {type: String},
            person: {type: Object}
        }
    }

    constructor() {
        super();

        this.person = {};
    }

    render() {
        return html`
        `;
    }

    // Se ejecuta al cambiar el valor de alguna de las propiedades
    updated(changedProperties) {
        console.log("updated viva-api-get-user");

        if (changedProperties.has("userid")) {
            this.getPersonData();
        }
    }

    getPersonData() {
        console.log("getPersonData");
        console.log("Consultamos datos de la persona");

        let url = "http://localhost:8081/viveawards/users/" + this.userid;
        console.log("URL: ", url);

        /*        
        // Datos mockeados
        console.log("Enviamos datos mockeados");
        this.dispatchEvent(new CustomEvent("viva-api-get-user-ok", {
            detail: {
                person: {
                    userid: "100",
                    name: "Moquito",
                    surname: "Prueba",
                    age: "5"                           
                }
            }
        }))
        */

        let xhr = new XMLHttpRequest();

        // Al ser una request asincrona este onload se ejecutará cuando el send (que está más
        // abajo) se complete (responda el API)
        xhr.onload = () => {
            console.log("Status: ", xhr.status);

            if (xhr.status === 200) {
                console.log("Petición completada correctamente");

                //console.log("xhr: ", xhr);
                let APIResponse = JSON.parse(xhr.responseText);

                console.log("APIResponse: ", APIResponse);
                this.person = APIResponse;

                this.dispatchEvent(new CustomEvent("viva-api-get-user-ok", {
                    detail: {
                        person: {
                            userid: this.person.id,
                            name: this.person.nombre,
                            surname: this.person.apellidos,
                            age: this.person.edad                           
                        }
                    }
                })
            )
            } else if (xhr.status === 404) {
                console.log("Petición persona no existe");
                console.log("xhr: ", xhr);

                this.dispatchEvent(new CustomEvent("viva-api-get-user-not-found", {}));
            }
        };

        xhr.onerror = (e) => {
            console.log(e);
            console.log("Status: ", xhr.status)
            alert("Unknown Error Occured. Server response not received.");
        };

        xhr.open("GET",url);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send();

        console.log("Fin de getPersonData");
    }
}

customElements.define("viva-api-get-user", VivaApiGetUser);
import { LitElement, html } from "lit-element";

class ViviaApiPostRequest extends LitElement {

    static get properties() {
        return {
            userid: {type: String},
            reward: {type: Object}
        }
    }

    constructor() {
        super();

    }

    render() {
        return html`
        `;
    }

    // Se ejecuta al cambiar el valor de alguna de las propiedades
    updated(changedProperties) {
        console.log("updated viva-post-request");

        if (changedProperties.has("userid")) {
            this.postRequest();
        }
    }

    postRequest() {
        console.log("postRequest");
        console.log("Enviando código de empleado");

        let data = JSON.stringify({ 
            "userId": this.userid
         });
        console.log("Data: ", data);

        let url = "http://localhost:8081/viveawards/requests/";

        let xhr = new XMLHttpRequest();

        // Al ser una request asincrona este onload se ejecutará cuando el send (que está más
        // abajo) se complete (responda el API)
        xhr.onload = () => {
            console.log("Status: ", xhr.status)
            if (xhr.status === 200) {
                console.log("Petición completada correctamente");

                //console.log("xhr: ", xhr);
                let APIResponse = JSON.parse(xhr.responseText);

                console.log("APIResponse: ", APIResponse);
                this.reward = APIResponse;

                this.sendResponseRequest();
            }
        };
        xhr.open("POST",url);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(data);

        console.log("Fin de postRequest");
    }

    sendResponseRequest() {
        console.log("sendResponseRequest");
        console.log(this.reward);

        this.dispatchEvent(new CustomEvent("send-respond-request", {
            detail: {
                reward: {
                    id: this.reward.id,
                    awards_name: this.reward.awards_name,
                    awards_desc: this.reward.awards_desc,
                 }
             }
        })
    )

    }
}

customElements.define("viva-api-post-request", ViviaApiPostRequest);
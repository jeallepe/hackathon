import { LitElement, html } from "lit-element";

class VivaFooter extends LitElement {

    static get properties() {
        return {
        }
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h5>@VivaApp 2021 (Footer)</h5>
        `;
    }
}

customElements.define("viva-footer", VivaFooter);
import { LitElement, html } from "lit-element";

class VivaShowError extends LitElement {

    static get properties() {
        return {
            message: {type: String}
        }
    }

    constructor() {
        super();

        this.message = "";
    }

    render() {
        return html`
            <h5>${this.message}</h5>
        `;
    }
}

customElements.define("viva-show-error", VivaShowError);